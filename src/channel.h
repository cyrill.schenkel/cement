/* channel.h -- typed lock free queues

   Copyright (C) 2024  Cyrill Schenkel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _CEMENT_CHANNEL_H
#define _CEMENT_CHANNEL_H

#include <stddef.h>

#include "allocator.h"

#ifdef __STDC_NO_ATOMICS__
#error "channels do not work without atomics"
#else
#include <stdatomic.h>
#endif

/**
 * Define a typed channel.
 *
 * Channels are lock free queues designed to facilitate message passing between
 * threads.
 *
 * This macro is designed to be used like an anonymous struct. Usually a user
 * would use this with a `typedef` clause.
 *
 * NOTE: Users must set the `allocator` field on any channel instance and call
 * `channel_init` on it.
 *
 * @param item type of messages held by this channel
 */
#define channel(item)                                                          \
  struct {                                                                     \
    allocator_t *allocator;                                                    \
    _Atomic(struct _channel_node_##item {                                      \
      item _item;                                                              \
      _Atomic(struct _channel_node_##item *) _next;                            \
    } *) _head,                                                                \
        _tail;                                                                 \
    struct _channel_node_##item *_null;                                        \
  }

/**
 * Channel initializer using default parameters.
 *
 * Sets `allocator` to `allocator_default`.
 */
#define channel_default()                                                      \
  { .allocator = &allocator_default }

/**
 * Initialize a channel.
 */
#define channel_init(channel_ptr)                                              \
  __extension__({                                                              \
    typeof(channel_ptr) _channel = (channel_ptr);                              \
    _channel->_head =                                                          \
        allocator_alloc(_channel->allocator, sizeof(_channel->_head));         \
    _channel->_head->_next = NULL;                                             \
    _channel->_tail = _channel->_head;                                         \
  })

/**
 * Send a message to any receiver.
 *
 * This procedure is thread safe.
 *
 * @param item the message
 */
#define channel_send(channel_ptr, item)                                        \
  __extension__({                                                              \
    typeof(channel_ptr) _channel = (channel_ptr);                              \
    typeof(_channel->_head->_item) _item = (item);                             \
    typeof(_channel->_tail) _new_item =                                        \
        allocator_alloc(_channel->allocator, sizeof(_channel->_tail));         \
    _new_item->_item = _item;                                                  \
    _new_item->_next = NULL;                                                   \
    typeof(_channel->_tail) _old_tail = _channel->_tail;                       \
    while (!atomic_compare_exchange_weak(&_old_tail->_next, &_channel->_null,  \
                                         _new_item))                           \
      _old_tail = _channel->_tail;                                             \
    atomic_compare_exchange_weak(                                              \
        &_channel->_tail, (typeof(_channel->_null) *)&_old_tail, _new_item);   \
  })

/**
 * Remove oldest message from the channel and return it in `out_ptr`.
 *
 * Only alters `out_ptr` if a message was received.
 *
 * @param out_ptr destination pointer for returning the message
 * @return 1 if a message was received and 0 otherwise
 */
#define channel_receive(channel_ptr, out_ptr)                                  \
  __extension__({                                                              \
    typeof(channel_ptr) _channel = (channel_ptr);                              \
    typeof(out_ptr) _out = (out_ptr);                                          \
    int _result = 0;                                                           \
    typeof(_channel->_head) _old_head = _channel->_head;                       \
    while (_old_head->_next &&                                                 \
           !atomic_compare_exchange_weak(                                      \
               &_channel->_head, (typeof(_channel->_null) *)&_old_head,        \
               _old_head->_next))                                              \
      _old_head = _channel->_head;                                             \
    if (_old_head->_next) {                                                    \
      _result = 1;                                                             \
      *_out = _old_head->_next->_item;                                         \
      allocator_free(_channel->allocator, _old_head);                          \
    }                                                                          \
    _result;                                                                   \
  })

/**
 * Free internal resources.
 *
 * NOTE: The procedure only frees internal allocations. Any external memory,
 * internal structures might point to, are unaffected.
 *
 * NOTE: Calling any procedure other than `channel_init` on the channel after
 * closing it results in undefined behavior.
 */
#define channel_close(channel_ptr)                                             \
  __extension__({                                                              \
    typeof(channel_ptr) _channel = (channel_ptr);                              \
    typeof(_channel->_head) _node = _channel->_head;                           \
    while (_node) {                                                            \
      typeof(_channel->_head) _next = _node->_next;                            \
      allocator_free(_channel->allocator, _node);                              \
      _node = _next;                                                           \
    }                                                                          \
    _channel->_head = NULL;                                                    \
    _channel->_tail = NULL;                                                    \
  })

#endif // _CEMENT_CHANNEL_H
