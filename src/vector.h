/* vector.h -- typed dynamic arrays

   Copyright (C) 2024  Cyrill Schenkel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _CEMENT_VECTOR_H
#define _CEMENT_VECTOR_H

#include <stddef.h>

#include "allocator.h"
#include "memory.h"
#include "slice.h"

#ifndef VECTOR_INITIAL_CAPACITY
#define VECTOR_INITIAL_CAPACITY 32
#endif

#ifndef VECTOR_GROWTH_FN
#define VECTOR_GROWTH_FN(capacity) (capacity) * 2
#endif

#ifdef VECTOR_DISABLE_INLINE_OPTIMIZATION
#define _vector_inline_capacity(item) 0
#else
#define _vector_inline_capacity(item)                                          \
  ((sizeof(size_t) + sizeof(item *)) / sizeof(item))
#endif

/**
 * Define a typed vector.
 *
 * This macro is designed to be used like an anonymous struct. Usually a user
 * would use this with a `typedef` clause.
 *
 * NOTE: Users must set the `allocator` field on any vector instance.
 *
 * @param item type of messages held by this vector
 */
#define vector(item)                                                           \
  struct {                                                                     \
    allocator_t *allocator;                                                    \
    size_t size;                                                               \
    union {                                                                    \
      struct {                                                                 \
        size_t capacity;                                                       \
        item *items;                                                           \
      };                                                                       \
      struct {                                                                 \
        __extension__ item inline_items[_vector_inline_capacity(item)];        \
      };                                                                       \
    } data;                                                                    \
  }

/**
 * Vector initializer using default parameters.
 *
 * Sets `allocator` to `allocator_default`.
 */
#define vector_default()                                                       \
  { .size = 0, .data.capacity = 0, .allocator = &allocator_default }

#define vector_get(vector_ptr, index)                                          \
  __extension__({                                                              \
    typeof(vector_ptr) _vector = (vector_ptr);                                 \
    typeof(_vector->data.items) _result;                                       \
    if (_vector->size <=                                                       \
        _vector_inline_capacity(typeof(*_vector->data.items))) {               \
      _result = &_vector->data.inline_items[(index)];                          \
    } else {                                                                   \
      _result = &_vector->data.items[(index)];                                 \
    }                                                                          \
    _result;                                                                   \
  })

/** @private */
#define _vector_ensure_capacity(_vector, min_size)                             \
  __extension__({                                                              \
    const size_t _min_size = (min_size);                                       \
    int _result = -1;                                                          \
    if (_vector->data.capacity < _min_size) {                                  \
      if (_vector->size <=                                                     \
              _vector_inline_capacity(typeof(*_vector->data.items)) &&         \
          _vector_inline_capacity(typeof(*_vector->data.items)) > 0) {         \
        const typeof(_vector->data.items) _items = allocator_alloc(            \
            _vector->allocator,                                                \
            VECTOR_INITIAL_CAPACITY * sizeof(*_vector->data.items));           \
        if (_items) {                                                          \
          memcpy(_items, _vector->data.inline_items, _vector->size);           \
          _vector->data.capacity = VECTOR_INITIAL_CAPACITY;                    \
          _vector->data.items = _items;                                        \
          _result = 0;                                                         \
        }                                                                      \
      } else {                                                                 \
        size_t _new_capacity = _vector->data.capacity == 0                     \
                                   ? VECTOR_INITIAL_CAPACITY                   \
                                   : _vector->data.capacity;                   \
        while (_new_capacity < min_size)                                       \
          _new_capacity = VECTOR_GROWTH_FN(_new_capacity);                     \
        typeof(_vector->data.items) _items =                                   \
            allocator_realloc(_vector->allocator, _vector->data.items,         \
                              _new_capacity * sizeof(*_vector->data.items));   \
        if (_items) {                                                          \
          _vector->data.items = _items;                                        \
          _vector->data.capacity = _new_capacity;                              \
          _result = 0;                                                         \
        }                                                                      \
      }                                                                        \
    } else {                                                                   \
      _result = 0;                                                             \
    }                                                                          \
    _result;                                                                   \
  })

/**
 * Add `item` to end of vector.
 */
#define vector_add(vector_ptr, item)                                           \
  __extension__({                                                              \
    int _result = 0;                                                           \
    const typeof(vector_ptr) _vector = (vector_ptr);                           \
    if (_vector->size + 1 <=                                                   \
        _vector_inline_capacity(typeof(*_vector->data.items))) {               \
      _vector->data.inline_items[_vector->size] = (item);                      \
      _vector->size++;                                                         \
    } else {                                                                   \
      _result = _vector_ensure_capacity(_vector, _vector->size + 1);           \
      if (!_result) {                                                          \
        _vector->data.items[_vector->size] = (item);                           \
        _vector->size++;                                                       \
      }                                                                        \
    }                                                                          \
    _result;                                                                   \
  })

/**
 * Add `count` items to end of vector.
 */
#define vector_add_all(vector_ptr, item_array, count)                          \
  __extension__({                                                              \
    int _result = 0;                                                           \
    const typeof(vector_ptr) restrict _vector = (vector_ptr);                  \
    const typeof(_vector->data.items) restrict _items = (item_array);          \
    const size_t _new_size = _vector->size + count;                            \
    if (_new_size <= _vector_inline_capacity(typeof(*_vector->data.items))) {  \
      memcpy(&_vector->data.inline_items[_vector->size], _items,               \
             count * sizeof(*_vector->data.items));                            \
      _vector->size = _new_size;                                               \
    } else {                                                                   \
      _result = _vector_ensure_capacity(_vector, _new_size);                   \
      if (!_result) {                                                          \
        memcpy(&_vector->data.items[_vector->size], _items,                    \
               count * sizeof(*_vector->data.items));                          \
        _vector->size = _new_size;                                             \
      }                                                                        \
    }                                                                          \
    _result;                                                                   \
  })

/**
 * Append `slice` to end of vector.
 */
#define vector_append_slice(vector_ptr, slice)                                 \
  __extension__({                                                              \
    const typeof(slice) _slice = (slice);                                      \
    vector_add_all((vector_ptr), _slice.items, _slice.size);                   \
  })

/**
 * Append `other` vector to end of this vector.
 */
#define vector_append_vector(vector_ptr, other_vector_ptr)                     \
  __extension__({                                                              \
    const typeof(vector_ptr) _this = (vector_ptr);                             \
    const typeof(vector_ptr) _other = (other_vector_ptr);                      \
    const typeof(_this->data.items) _items =                                   \
        _other->size > _vector_inline_capacity(typeof(*_this->data.items))     \
            ? _other->data.items                                               \
            : _other->data.inline_items;                                       \
    vector_add_all(_this, _items, _other->size);                               \
  })

/**
 * Remove item at `index` from vector.
 *
 * This will copy all items after `index`. If the order of items is irrelevant,
 * use `vector_swap_remove` instead.
 */
#define vector_remove(vector_ptr, index)                                       \
  __extension__({                                                              \
    int _result = 0;                                                           \
    const typeof(vector_ptr) _vector = (vector_ptr);                           \
    const size_t _index = (index);                                             \
    const size_t _inline_capacity =                                            \
        _vector_inline_capacity(typeof(*_vector->data.items));                 \
    const size_t _new_size = _vector->size - 1;                                \
    if (_index < _vector->size) {                               \
      if (_new_size < _inline_capacity) {                                      \
        memmove(&_vector->data.inline_items[_index],                           \
                &_vector->data.inline_items[_index + 1],                       \
                (_vector->size - _index - 1) * sizeof(*_vector->data.items));  \
      } else if (_new_size == _inline_capacity) {                              \
        const typeof(_vector->data.items) _items = _vector->data.items;        \
        memcpy(_vector->data.inline_items, _items, _index * sizeof(*_items));  \
        memcpy(&_vector->data.inline_items[_index], &_items[_index + 1],       \
               (_vector->size - _index - 1) * sizeof(*_vector->data.items));   \
        allocator_free(_vector->allocator, _items);                            \
      } else {                                                                 \
        memmove(&_vector->data.items[_index],                                  \
                &_vector->data.items[_index + 1],                              \
                (_vector->size - _index - 1) * sizeof(*_vector->data.items));  \
      }                                                                        \
      _vector->size--;                                                         \
    } else {                                                                   \
      _result = -1;                                                            \
    }                                                                          \
    _result;                                                                   \
  })

/**
 * Remove item at `index` by replacing it with last item in vector.
 */
#define vector_swap_remove(vector_ptr, index)                                  \
  __extension__({                                                              \
    int _result = 0;                                                           \
    const typeof(vector_ptr) _vector = (vector_ptr);                           \
    const size_t _index = (index);                                             \
    const size_t _inline_capacity =                                            \
        _vector_inline_capacity(typeof(*_vector->data.items));                 \
    const size_t _new_size = _vector->size - 1;                                \
    if (_index < _vector->size) {                               \
      if (_index < _new_size && _new_size < _inline_capacity) {                \
        _vector->data.inline_items[_index] =                                   \
            _vector->data.inline_items[_vector->size - 1];                     \
      } else if (_new_size == _inline_capacity) {                              \
        const typeof(_vector->data.items) _items = _vector->data.items;        \
        memcpy(_vector->data.inline_items, _items, _index * sizeof(*_items));  \
        memcpy(&_vector->data.inline_items[_index], &_items[_index + 1],       \
               (_vector->size - _index - 1) * sizeof(*_vector->data.items));   \
        allocator_free(_vector->allocator, _items);                            \
      } else if (_index < _new_size) {                                         \
        _vector->data.items[_index] = _vector->data.items[_vector->size - 1];  \
      }                                                                        \
      _vector->size--;                                                         \
    } else {                                                                   \
      _result = -1;                                                            \
    }                                                                          \
    _result;                                                                   \
  })

/**
 * Remove all items.
 *
 * Frees internally allocated memory.
 */
#define vector_clear(vector_ptr)                                               \
  __extension__({                                                              \
    const typeof(vector_ptr) _vector = (vector_ptr);                           \
    if (_vector->size >                                                        \
        _vector_inline_capacity(typeof(*_vector->data.items))) {               \
      allocator_free(_vector->allocator, _vector->data.items);                 \
    }                                                                          \
    _vector->size = 0;                                                         \
  });

/**
 * Define new vector type and related procedures.
 *
 * The procedures are named similar to `vector_` macros, but using `name`
 * instead of `vector`.
 *
 * @param item type of items held by this vector
 * @param name type of the vector
 */
#define def_vector(item, name)                                                 \
  typedef vector(item) name;                                                   \
  item *name##_get(name *vector, const size_t index) {                         \
    return vector_get(vector, index);                                          \
  }                                                                            \
  int name##_add(name *vector, const item value) {                             \
    return vector_add(vector, value);                                          \
  }                                                                            \
  int name##_add_all(name *vector, const item *values, const size_t count) {   \
    return vector_add_all(vector, values, count);                              \
  }                                                                            \
  int name##_append_slice(name *vector, const any_slice slice) {               \
    return vector_append_slice(vector, slice);                                 \
  }                                                                            \
  int name##_append_vector(name *vector, const name *other) {                  \
    return vector_append_vector(vector, other);                                \
  }                                                                            \
  int name##_remove(name *vector, const size_t index) {                        \
    return vector_remove(vector, index);                                       \
  }                                                                            \
  int name##_swap_remove(name *vector, const size_t index) {                   \
    return vector_swap_remove(vector, index);                                  \
  }                                                                            \
  int name##_clear(name *vector) { return vector_clear(vector); }

#endif // _CEMENT_VECTOR_H
