/* string.h -- string utilities

   Copyright (C) 2024  Cyrill Schenkel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _CEMENT_STRING_H
#define _CEMENT_STRING_H

#include "slice.h"

def_slice(unsigned char, string)

#define string(const_cstr)                                                     \
  ((string){.size = sizeof(const_cstr), .items = (unsigned char *)const_cstr})

#endif // _CEMENT_STRING_H
