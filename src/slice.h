/* slice.h -- typed slice

   Copyright (C) 2024  Cyrill Schenkel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _CEMENT_SLICE_H
#define _CEMENT_SLICE_H

#include <stddef.h>

#define slice(item)                                                            \
  struct {                                                                     \
    size_t size;                                                               \
    item *items;                                                               \
  }

#define def_slice(item, name) typedef slice(item) name;

def_slice(void, any_slice)

#define slice_to_any(slice)                                                    \
  __extension__({                                                              \
    const typeof(slice) _slice = (slice);                                      \
    const any_slice _any = {.size = _slice.size, .items = _slice.items};       \
    _any;                                                                      \
  })

#endif // _CEMENT_SLICE_H
