/* allocator.h -- define memory allocator interface

   Copyright (C) 2024  Cyrill Schenkel

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _CEMENT_ALLOCATOR_H
#define _CEMENT_ALLOCATOR_H

#include <stddef.h>

/**
 * Pass allocators to procedures in order to control their memory allocation
 * behavior.
 */
typedef struct allocator {
  const void *state;
  void *(*alloc)(void *state, const size_t size);
  void *(*realloc)(void *restrict state, void *restrict ptr, const size_t size);
  void (*free)(void *state, void *ptr);
} allocator_t;

/** Allocate `size` bytes of memory using `allocator`. */
inline void *allocator_alloc(allocator_t *allocator, const size_t size) {
  return allocator->alloc(&allocator->state, size);
}

/**
 * Change the size of the block of memory pointed to by `ptr` to `size` using
 * `allocator`.
 *
 * @param ptr must have been allocated by `allocator`
 */
inline void *allocator_realloc(allocator_t *restrict allocator,
                               void *restrict ptr, const size_t size) {
  return allocator->realloc(&allocator->state, ptr, size);
}

/**
 * Free the block of memory pointed to by `ptr`.
 *
 * @param ptr must have been allocated by `allocator`
 */
inline void allocator_free(allocator_t *restrict allocator,
                           void *restrict ptr) {
  allocator->free(&allocator->state, ptr);
}

#if __STDC_HOSTED__
#include <stdlib.h>

void *_cement_allocator_stdlib_alloc(void *, const size_t size) {
  return malloc(size);
}

void *_cement_allocator_stdlib_realloc(void *restrict, void *restrict ptr,
                                       const size_t size) {
  return realloc(ptr, size);
}

void _cement_allocator_stdlib_free(void *restrict, void *restrict ptr) {
  free(ptr);
}

/** Default allocator. */
extern allocator_t allocator_default;

allocator_t allocator_default = {.state = NULL,
                                 .alloc = _cement_allocator_stdlib_alloc,
                                 .realloc = _cement_allocator_stdlib_realloc,
                                 .free = _cement_allocator_stdlib_free};
#endif // __STDC_HOSTED__

#endif // _CEMENT_ALLOCATOR_H
