#include "allocator.h"

void test_default_allocator(void) {
  allocator_t *allocator = &allocator_default;
  void *ptr = allocator_alloc(allocator, 2048);
  allocator_realloc(allocator, ptr, 4069);
  allocator_free(allocator, ptr);
}

int main(void) {
  test_default_allocator();
  return 0;
}
