#include <assert.h>

#include "string.h"

static void test_string_constant() {
  string msg = string("Hello, world!");
  assert(msg.size == 14);
}

int main(void) {
  test_string_constant();
  return 0;
}
