#include <assert.h>

#include "channel.h"

void test_int_channel(void) {
  channel(int) int_channel = channel_default();
  channel_init(&int_channel);
  channel_send(&int_channel, 5);
  channel_send(&int_channel, 12);
  int out = 0;
  assert(channel_receive(&int_channel, &out));
  assert(out == 5);
  assert(channel_receive(&int_channel, &out));
  assert(out == 12);
  assert(!channel_receive(&int_channel, &out));
  assert(out == 12);
  channel_close(&int_channel);
}

char fn0(char x) { return x + 1; }
char fn1(char x) { return x * 2; }

void test_fn_ptr_channel(void) {
  typedef char (*fn)(char);
  channel(fn) fn_channel = channel_default();
  channel_init(&fn_channel);
  fn out = NULL;
  assert(!channel_receive(&fn_channel, &out));
  channel_send(&fn_channel, fn0);
  assert(channel_receive(&fn_channel, &out));
  assert(out(2) == 3);
  channel_send(&fn_channel, fn1);
  assert(channel_receive(&fn_channel, &out));
  assert(out(2) == 4);
  channel_close(&fn_channel);
}

int main(void) {
  test_int_channel();
  test_fn_ptr_channel();
  return 0;
}
