#include <assert.h>
#include <stdint.h>

#include "slice.h"
#include "vector.h"

def_vector(int, ivec)

static void test_vector_small_elements(void) {
  vector(uint8_t) vec = vector_default();
  vector_add(&vec, 5);
  assert(*vector_get(&vec, 0) == 5);
  uint8_t items[] = {0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11,
                     12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23, 24,
                     25, 26, 27, 28, 29, 30, 31, 32, 33, 34};
  vector_add_all(&vec, items, sizeof(items) / sizeof(uint8_t));
  for (size_t i = 1; i < sizeof(items) / sizeof(uint8_t) + 1; i++)
    assert(*vector_get(&vec, i) == items[i - 1]);
  vector_clear(&vec);
}

static void test_vector_defined(void) {
  ivec vec = vector_default();
  ivec_add(&vec, 9);
  assert(*ivec_get(&vec, 0) == 9);
  ivec_clear(&vec);
}

static void test_vector_remove(void) {
  ivec vec = vector_default();
  int data[] = {1, 2, 3, 4, 5, 6};
  ivec_add_all(&vec, data, sizeof(data) / sizeof(int));
  ivec_remove(&vec, 0);
  assert(*vector_get(&vec, 0) == 2);
  assert(*vector_get(&vec, 1) == 3);
  assert(*vector_get(&vec, 4) == 6);
  ivec_remove(&vec, 4);
  assert(*vector_get(&vec, 3) == 5);
  ivec_remove(&vec, 1);
  assert(*vector_get(&vec, 0) == 2);
  assert(*vector_get(&vec, 1) == 4);
  ivec_remove(&vec, 2);
  ivec_remove(&vec, 1);
  ivec_remove(&vec, 0);
  assert(vec.size == 0);
  ivec_clear(&vec);
}

static void test_append_slice(void) {
  int data[] = {1, 2, 3, 4, 5};
  slice(int) values = {5, data};
  ivec vec = vector_default();
  ivec_append_slice(&vec, slice_to_any(values));
  assert(vec.size == 5);
}

static void test_append_vector(void) {
  ivec a = vector_default();
  ivec b = vector_default();
  ivec_add(&a, 3);
  ivec_add(&b, 6);
  ivec_append_vector(&a, &b);
  assert(b.size == 1);
  assert(a.size == 2);
  assert(*vector_get(&a, 0) == 3);
  assert(*vector_get(&a, 1) == 6);
}

static void test_vector_swap_remove(void) {
  ivec vec = vector_default();
  int data[] = {1, 2, 3, 4, 5, 6};
  ivec_add_all(&vec, data, sizeof(data) / sizeof(int));
  ivec_swap_remove(&vec, 0);
  assert(*vector_get(&vec, 0) == 6);
  assert(*vector_get(&vec, 1) == 2);
  assert(*vector_get(&vec, 4) == 5);
  ivec_swap_remove(&vec, 4);
  assert(*vector_get(&vec, 3) == 4);
  ivec_swap_remove(&vec, 1);
  assert(*vector_get(&vec, 0) == 6);
  assert(*vector_get(&vec, 1) == 4);
  ivec_swap_remove(&vec, 2);
  ivec_swap_remove(&vec, 1);
  ivec_swap_remove(&vec, 0);
  assert(vec.size == 0);
  ivec_clear(&vec);
}

static void test_vector_clear(void) {
  ivec vec = vector_default();
  int data[] = {1, 2, 3, 4, 5, 6};
  ivec_add_all(&vec, data, sizeof(data) / sizeof(int));
  ivec_clear(&vec);
  assert(vec.size == 0);
}

struct large {
  uint64_t a, b, c, d;
};

static int large_eq(const struct large *restrict x,
                    const struct large *restrict y) {
  return x->a == y->a && x->b == y->b && x->c == y->c && x->d == y->d;
}

static void test_vector_large_elements(void) {
  vector(struct large) vec = vector_default();
  struct large x = {1, 2, 3, 4};
  struct large y = {12, 34, 56, 78};
  struct large z[50] = {0};
  struct large zero = {0};
  vector_add(&vec, x);
  assert(large_eq(vector_get(&vec, 0), &x));
  vector_add(&vec, y);
  assert(large_eq(vector_get(&vec, 1), &y));
  vector_add_all(&vec, z, 50);
  for (size_t i = 2; i < sizeof(z) / sizeof(struct large) + 2; i++)
    assert(large_eq(vector_get(&vec, i), &zero));
  vector_clear(&vec);
}

int main(void) {
  test_vector_small_elements();
  test_vector_large_elements();
  test_vector_defined();
  test_vector_remove();
  test_append_slice();
  test_vector_swap_remove();
  test_vector_clear();
  return 0;
}
