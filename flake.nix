{
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        cement = pkgs.callPackage ./package.nix {};
      in
      {
        packages.default = cement;

        devShells.default = with pkgs; mkShell {
          packages = [
            cement
            cmake
            doxygen
            gdb
            graphviz
            ninja
          ];

          buildInputs = [
          ];

          CMAKE_EXPORT_COMPILE_COMMANDS = "1";
        };
      }
    );
}
