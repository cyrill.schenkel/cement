{ stdenv
, lib
, cmake
}:

stdenv.mkDerivation {
  pname = "cement";
  version = "1.0";

  src = ./.;

  nativeBuildInputs = [ cmake ];

  enableParallelBuilding = true;

  doCheck = true;

  installPhase = ''
    mkdir -p $out
    echo cannot be installed yet > $out/README
  '';

  meta = with lib; {
    description = "Various helpers for writing C programs.";
    platforms = platforms.linux;
  };
}
